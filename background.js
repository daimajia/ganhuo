;
(function(undefined) {
    function executeScript(tab) {
        chrome.tabs.executeScript(tab.id, {
            "file": "js/contentScript.js"
        });
    }
    chrome.browserAction.onClicked.addListener(function(tab) {
        executeScript(tab);
    });

    // The onClicked callback function.
    function onClickHandler(info, tab) {
        if (info.menuItemId == "shareGanhuo") {
            executeScript(tab);
        }
    };

    chrome.contextMenus.onClicked.addListener(onClickHandler);

    // Set up context menu tree at install time.
    chrome.runtime.onInstalled.addListener(function() {
        // Create one test item for each context type.
        var contexts = ["page", "selection", "link", "editable", "image", "video", "audio"];
        chrome.contextMenus.create({
            "title": "提交到Gank.IO",
            "id": "shareGanhuo",
            "contexts": contexts
        });
    });
})();
