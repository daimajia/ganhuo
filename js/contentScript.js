;
(function() {
    var gh_modal;

    function gh_check_exist() {
      $.ajax({
        type: 'post',
        url: 'https://gank.io/api/exist',
        dataType: 'json',
        data: {
          url: window.location.href
        }
      }).done(function(data){
        if(data.error){
          toastr.error("干货战斗小组", data.msg)
        }else{
          $('#gh_exist').html(data.msg);
        }
      });
    }

    function gh_close_modal() {
        gh_modal.close();
        $("#gh_modal_wrapper").remove();
    }

    function submit_ganhuo() {
        var content = $("#gh_description").val();
        var url = window.location.url;
        var type = $("#gh_type").val();
        var who = $("#gh_who").val();

        if (!content || content.length == 0) {
            alert('请填写描述语');
            return;
        }

        if (!who || who.length == 0) {
            alert('请填写你的 ID');
            return;
        }
        $.ajax({
            type: 'POST',
            url: 'https://gank.io/api/add',
            data: {
                url: window.location.href,
                desc: content,
                who: who,
                type: type
            }
        }).done(function(data){
            if(data.error){
                toastr.error("干货战斗小组", data.msg)
            }else{
                gh_modal.close();
                toastr.success("干货战斗小组", data.msg);
            }
        }).fail(function(){
            toastr.error("干货战斗小组", "联网出问题了");
        });

    }

    if(!document.getElementById("gh_modal_wrapper")) {
       $.get(chrome.extension.getURL('modal.html'), function(data) {
        $("body").append(data);
        gh_modal = new RModal(document.getElementById("gh_modal_wrapper"), {
            beforeOpen: function(next) {
                next();
            },
            afterOpen: function() {
                gh_check_exist();
                $('#gh_close').click(gh_close_modal);
                $('#gh_submit').click(submit_ganhuo);
                var gh_link_selector = $("#gh_link");
                gh_link_selector.attr('href', window.location.href);
                gh_link_selector.html(window.location.href);
            },
            beforeClose: function(next) {
                next();
            },
            afterClose: function() {

            },
            dialogClass: "gh_modal",
            bodyClass: "gh_modal_wrapper"
        });
        gh_modal.open();
    });
    }
})();
